function onChangedSelectForm(){
	createInputArea(createCipherObject());
	document.getElementById("output_text").value = "";
}

function createCipherObject(){
	const cipherIndex = document.select_cipher_form.select_cipher_options.selectedIndex;
	let cipherObject = new Object();

	switch(cipherIndex){
		case 1:
			cipherObject.name = "atbash";
			cipherObject.title = "Atbash cipher";
			cipherObject.doesHaveKey = false;
			cipherObject.isReversible = true;
			cipherObject.description = `
				<p>
					アトバシュ暗号(Atbash cipher)は、ヘブライ文字を暗号化するために用いられていたといわれる単一換字式暗号の一種です。<br>
				</p>
				<p>
					[暗号化の仕組み]<br>
					暗号化の仕組みは単純です。アルファベットを一列に並べ、最初の文字は最後の文字へ変換、<br>
					最初から２番目の文字は最後から２番目の文字へ変換、といったように続けていきます。<br>
				</p>
				${createConversionTable(0, 0, 0, "アトバシュ暗号変換表")}<br>
				<p>
					"A"は"Z"へ、"B"は"Y"へ、"C"は"X"へ、と1文字ずつ対応しているのがわかるとおもいます。<br>
				</p>
				<p>
					[暗号化の例]<br>
					日本語で"暗号"という意味を持つ、英単語"CIPHER"は、<br>
					アトバシュ暗号を用いるとどのような文字列になるでしょう？<br>
				</p>
				${createConversionTable(0, 0, 3, "C → X")}<br>
				${createConversionTable(0, 0, 9, "I → R")}<br>
				${createConversionTable(0, 0, 16, "P → K")}<br>
				${createConversionTable(0, 0, 8, "H → S")}<br>
				${createConversionTable(0, 0, 5, "E → V")}<br>
				${createConversionTable(0, 0, 18, "R → I")}<br>
				<p>
					"CIPHER"は"XRKSVI"という一見無意味な文字列へと変換することができました！
				</p>
				<p>
					[情報元]
					Atbash (Jan. 4, 2017, 07:00 UTC). In Wikipedia: The Free Encyclopedia.
					Retrieved from https://en.wikipedia.org/wiki/Atbash<br>
				</p>
			`;
		break;

		case 2:
			cipherObject.name = "caeser";
			cipherObject.title = "Caeser cipher";
			cipherObject.doesHaveKey = true;
			cipherObject.isReversible = false;
			cipherObject.description = `
				<p>
					シーザー暗号(Caeser cipher)は、ローマの軍事的指導者カエサル（英語読みでシーザー）が使用していたといわれる単一換字式暗号の一種です。<br>
				</p>
				<p>
					[暗号化の仕組み]<br>
					平文の各文字を3文字ずらすことによって暗号化します。<br>
				</p>
				${createConversionTable(3, 3, 0, "シーザー暗号変換表 (鍵が3の場合)")}<br>
				<p>
					ずらす数は3以外の数字でもよく、この数字のことを"鍵"といいます。<br>
				</p>
				${createConversionTable(3, 13, 0, "シーザー暗号変換表 (鍵が13の場合)")}<br>
				<p>
					ただ、アルファベットは26文字のため、実質的にシーザー暗号のパターンは26個に限られます。<br>
					下の表(鍵:39)は、26文字ずらした段階で最初の状態に戻ってしまうため、<br>
					39 - 26 = 13で、鍵が13の場合と同じ変換表になってしまいました。<br>
				</p>
				${createConversionTable(3, 39, 0, "シーザー暗号変換表 (鍵が39の場合)")}<br>
				<p>
					[暗号化の例]<br>
					英単語"CIPHER"は、シーザー暗号 (鍵:15)を用いるとどのような文字列になるでしょう？<br>
				</p>
				${createConversionTable(3, 15, 3, "C → R (鍵:15)")}<br>
				${createConversionTable(3, 15, 9, "I → X (鍵:15)")}<br>
				${createConversionTable(3, 15, 16,"P → E (鍵:15)")}<br>
				${createConversionTable(3, 15, 8, "H → W (鍵:15)")}<br>
				${createConversionTable(3, 15, 5, "E → T (鍵:15)")}<br>
				${createConversionTable(3, 15, 18, "R → G (鍵:15)")}<br>
				<p>
					"CIPHER"は"RXEWTG"という一見無意味な文字列へと変換することができました！
				</p>
				<p>
					[情報元]
					Caeser cipher (Jan. 4, 2017, 07:00 UTC). In Wikipedia: The Free Encyclopedia.
					Retrieved from https://en.wikipedia.org/wiki/Caesar_cipher<br>
				</p>
			`;
		break;

		case 3:
			cipherObject.name = "vigenere";
			cipherObject.title = "Vigenère cipher";
			cipherObject.doesHaveKey = true;
			cipherObject.isReversible = false;
			cipherObject.description = `
				<p>
					ヴィジュネル暗号(Vigenère cipher)は、シーザー暗号をベースにした多表式換字暗号の一種です。<br>
				</p>
				<p>
					[暗号化の仕組み]<br>
					ヴィジュネル暗号は、異なる鍵を持つ連続したシーザー暗号と考えるとわかりやすいかもしれません。<br>
					シーザー暗号は、全ての文字に対して、同じ鍵を適用します。<br>
					例えば鍵が3のシーザー暗号では、"ABCDE"という文字列は全て3文字ずれて"DEFGH"という文字列へと変換されます。<br>
				</p>
				${createConversionTable(3, 3, 0, "ABCDE → DEFGH")}<br>
				<p>
					これに対して、ヴィジュネル暗号は文字ごとにずらす数が変化します。<br>
				</p>
				<p>
					[実践]<br>
					例えば、鍵が"key"という文字列で、"CIPHER"という文字列を暗号化することを考えてみます。<br>
					"key"の"K"というアルファベットは、Aを0として最初から数えて10番目。"E"は4番目、"Y"は24番目です。<br>
					この"11, 5, 25"という数字を鍵として、"CIPHER"という文字列を順番に変換していきます。<br>
				</p>
				${createConversionTable(3, 10, 3, "C → M(鍵:k = 10)")}<br>
				${createConversionTable(3, 4, 9, "I → M(鍵:e = 4)")}<br>
				${createConversionTable(3, 24, 16,"P → N(鍵:z = 24)")}<br>
				${createConversionTable(3, 10, 8, "H → R(鍵:k = 10)")}<br>
				${createConversionTable(3, 4, 5, "E → I(鍵:e = 4)")}<br>
				${createConversionTable(3, 24, 18, "R → P(鍵:y = 24)")}<br>
				<p>
					"CIPHER"は"MMNRIP"へと変換されました。<br>
				</p>
				<p>
					[情報元]<br>
					Vigenère cipher (Jan. 6, 2017, 07:00 UTC). In Wikipedia: The Free Encyclopedia.
					Retrieved from https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher<br>
				</p>
			`;
		break;

		case 4:
			cipherObject.name = "a1z26";
			cipherObject.title = "A1Z26";
			cipherObject.doesHaveKey = false;
			cipherObject.isReversible = false;
			cipherObject.description = `
				<p>
					A1Z26暗号(A1Z26 cipher)は、アニメ「怪奇ゾーン　グラビティフォールズ」(原題:Gravity Falls)で用いられた暗号です。<br>
					昔の暗号というわけではないので、このWEBサイトの趣旨とは外れますが、簡単に紹介します。<br>
				</p>
				<p>
					[暗号化の仕組み]<br>
					A1Z26暗号は、アルファベットを順に並べ、"A"を"1"、"B"を"2"、"C"を"3"...というように置き換えます。<br>
				</p>
				${createConversionTable(1, 0, 0, "A1Z26")}<br>
				<p>
					文字と文字の間にはハイフン("-")を置き、単語の区切りにはスペース(" ")を用います。<br>
				</p>
				<p>
					"I like it"という文章は"9 12-9-11-5 9-20"というように表せます。<br>
					平文の文字が大文字か小文字かという情報は失われます。<br>
				</p>
			`;
		break;

		case 5:
			cipherObject.name = "binary";
			cipherObject.title = "Binary convert";
			cipherObject.doesHaveKey = false;
			cipherObject.isReversible = false;
			cipherObject.description = `
				<p>
					バイナリ(binary)とは、二進法のことです。<br>
					これは暗号ではありませんが、作ってみたので置いてみました。<br>
				</p>
			`;
		break;

		default:
			cipherObject.name = "not_selected";
			cipherObject.title = "下のプルダウンメニューから、暗号を選択してください";
			cipherObject.doesHaveKey = false;
			cipherObject.isReversible = true;
			cipherObject.description = `
				<p>
					むかしの暗号を紹介するWEBサービスです。<br>
					主に現代では使われていない、歴史のある暗号を簡単に紹介します。<br>
					現在対応している言語は英語のみです。<br>
					利用の際には、以下のご注意点をお読みいただき、了解いただいてからご利用ください。<br>
				</p>
				<p>
					[注意]<br>
					このサイトで紹介されている暗号は、現代では一瞬で解読されてしまうような弱いものばかりです。<br>
					決して他サイトのパスワードなどへの利用はしないでください。<br>
					また、暗号化・復号のアルゴリズムや暗号に関する情報は、誤ったものである可能性もあります。<br>
					当サイトを利用することによって生じたいかなるトラブル・損失・損害に対しても、当サイトは責任を負いません。<br>
				</p>
			`;
		break;
	}
	return cipherObject;
}

function createConversionTable(cipherType = 0, shiftNum = 0, checkColumn = 0, title = ""){
	let firstRow = "";
	let secondRow = "";

	for(let i=0; i<26;i++){
		let decCh = 65 + i;
		let td = "<td class='ct'>" + String.fromCharCode(decCh) + "</td>";
		if(checkColumn - 1 == i){
			td = "<td class='ct checkColumn'>" + String.fromCharCode(decCh) + "</td>";
		}
		firstRow += td;
	}

	for(let i=0; i<26;i++){
		let decCh = 0;
		let insideCell = "";
		switch(cipherType){
			case 0://atbash
				decCh = 90 - i;
				insideCell = String.fromCharCode(decCh);
				break;
			case 1://a1z26
				insideCell = (i + 1);
				break;
			default:
				decCh = 65 + i + shiftNum;
				while(decCh > 90){
					decCh -= 26;
				}
				insideCell = String.fromCharCode(decCh);
				break;
		}

		let td = "<td class='ct'>" + insideCell + "</td>";
		if(checkColumn - 1 == i){
			td = "<td class='ct checkColumn'>" + insideCell + "</td>";
		}
		secondRow += td;
	}

	let result = "<table id='commentary_table' border='1' width='100%'><tr>" +
				"<caption>" + title + "</caption>" +
				"<td class='ct'>平</td>" +
				firstRow + 
				"</tr><tr><td class= 'ct'>暗</td>" + 
				secondRow + 
				"</tr></table>";
	return result;
}

function createInputArea(cipherObject){
	const cipherTitle = document.getElementById("cipher_title");
	const convertButton = document.getElementById("button_input");
	const commentaryArea = document.getElementById("commentary_area");
	document.getElementById("caeser_input").style.display = "none";
	document.getElementById("vigenere_input").style.display = "none";

	//鍵が必要な暗号は、鍵を入力するエリアを表示する
	if(cipherObject.doesHaveKey){
		document.getElementById(cipherObject.name + "_input").style.display = "inline";
	}

	let text = '<button onClick="execute(';
	if(cipherObject.isReversible){
		text += "'";
		text += cipherObject.name;
		text += "');";
		text += '" class="btn btn-default play">変換</button>';
	}else{
		text += "'";
		text += "encode_" + cipherObject.name;
		text += "');";
		text += '" class="btn btn-default play">暗号化</button>';
		text += '<button onClick="execute(';
		text += "'";
		text += "decode_" + cipherObject.name;
		text += "');";
		text += '" class="btn btn-default play">復号</button>';		
	}
	cipherTitle.innerHTML = "<p>" + cipherObject.title + "</p>";
	convertButton.innerHTML = text;
	commentaryArea.innerHTML = cipherObject.description;
}

function execute(cipherName){
	let isDecryption = false;
	let encryptedText = "";
	let keyStr = "";
	const plainText = document.getElementById("input_text").value;
	switch(cipherName){
		case "atbash":
			encryptedText = convertAtbash(plainText);
			break;

		case "decode_caeser":
			isDecryption = true;
			/*FALLTHROUGH*/
		case "encode_caeser":
			keyStr = document.getElementById("caeser_key").value;
			let keyNum = 0;

			if(keyStr != undefined){
				keyStr = keyStr.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
					return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
				});
			}
			if(isNaN(keyStr)){
				encryptedText = "エラー: 鍵に数値を入力してください";
			}else{
				keyNum = parseInt(keyStr);
			}
			encryptedText = convertCaeser(plainText, keyNum, isDecryption);
			break;

		case "decode_vigenere":
			isDecryption = true;
			/*FALLTHROUGH*/
		case "encode_vigenere":
			keyStr = document.getElementById("vigenere_key").value;

			if(keyStr != undefined){
				keyStr = keyStr.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
					return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
				});
			}
			encryptedText = convertVigenere(plainText, keyStr, isDecryption);
			break;

		case "decode_a1z26":
			isDecryption = true;
			/*FALLTHROUGH*/
		case "encode_a1z26":
			if(!isDecryption){
				encryptedText = encodeA1z26(plainText);
			}else{
				encryptedText = decodeA1z26(plainText)
			}
			break;

		case "decode_binary":
			isDecryption = true;
			/*FALLTHROUGH*/
		case "encode_binary":
			if(!isDecryption){
				encryptedText = encodeBinary(plainText);
			}else{
				encryptedText = decodeBinary(plainText)
			}
			break;
	}
	document.getElementById("output_text").value = encryptedText;
}