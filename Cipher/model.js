//'A'と'a'をそれぞれ10進数に変換した数値
const DEC_CAP_A = 65;
const DEC_SML_A = 97;

/*
*	平文をアトバシュ暗号化する
*
* 	@param {string} text 平文
*
*	@return {string}
*/
function convertAtbash(text){
	let convertedText = "";

	const textLen = text.length;
	for(let i=0; i<textLen; i++){
		let decCh = text[i].charCodeAt(0);

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			decCh = 2 * DEC_CAP_A + 25 - decCh;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			decCh = 2 * DEC_SML_A + 25 - decCh;
		}

		convertedText += String.fromCharCode(decCh);
	}

	return convertedText;
}


/*
*	平文をシーザー暗号化する
*
* 	@param {string} text 平文
*	@param {int} c_key シーザー暗号の鍵(半角数0~25)
*
*	@return {string}
*/
function convertCaeser(text, c_key, isDecryption){
	let convertedText = "";
	let key = 0;
	if(typeof c_key == "string"){
		key = parseInt(c_key);
	}else{
		key = c_key;
	}

	if(isDecryption == true)
		key = -key;

	for(let i=0;i<text.length;i++){
		let decCh = text[i].charCodeAt(0);
		let caseSensitiveNum = 0;

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			caseSensitiveNum = DEC_CAP_A;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			caseSensitiveNum = DEC_SML_A;
		}

		if(caseSensitiveNum != 0){//text[i]がアルファベットであるとき
			if(key < 0){ key = 26 + key; }
			decCh = decCh + key;
			decCh -= caseSensitiveNum;
			decCh = decCh % 26;			//数値が0~25になるようにする

			decCh += caseSensitiveNum;
		}


		convertedText += String.fromCharCode(decCh);

	}
	return convertedText;
}

/*
*	平文(ptext)を鍵(ktext)を用いてビジュネル暗号化する
*	
*	ktextのアルファベットを一文字ずつ、0から25までの数値に変換して、
*	シーザー暗号の関数へ鍵として渡す
*	(例：鍵の1文字目がCである場合、平文の1文字目を2つずらす)
*	ktextが最後の文字までいった場合、ループする
*
*	isDecryptionがTrueの場合、暗号を平文化する
*
* 	@param {string} ptext 平文
*	@param {string} ktext 鍵テキスト
*	@param {bool} isDecryption
*
*	@return {string}
*/
function convertVigenere(ptext, ktext, isDecryption){
	let convertedText = "";
	let keyCount = 0;

	const ptextLen = ptext.length;
	for(let i=0; i<ptextLen; i++){
		let decKey = ktext[keyCount].charCodeAt(0);
		if(decKey >= DEC_CAP_A && decKey < DEC_CAP_A + 26){//大文字のとき
			decKey = decKey - DEC_CAP_A;
		}else if(decKey >= DEC_SML_A && decKey < DEC_SML_A + 26){//小文字のとき
			decKey = decKey - DEC_SML_A;
		}

		if(isDecryption)
			decKey = -decKey;

		convertedText += convertCaeser(ptext[i], decKey);

		if(ptext[i] != " "){
			keyCount++;
			if(keyCount >= ktext.length)
				keyCount = 0;
		}
	}

	return convertedText;
}
function convertVigenereChar(plainChar, charNumber, isDecryption){
	let convertedText = "";
	let key = changeVigenereKey(charNumber);
	if(isDecryption)
		key = -key;

	convertedText += convertCaeser(plainChar, key);

	return convertedText;
}


/*
*	平文をA1Z26で暗号化する
*
*	一つの単語はハイフンで繋がれ、スペースによって区切られる
*
* 	@param {string} text 平文
*
*	@return {string}
*/
function encodeA1z26(text){
	let convertedText = "";

	for(var i=0;i<text.length;i++){
		//テキスト1文字目ではなく、現在処理する文字と一つ前の文字がスペースでない場合、
		//ハイフンを追加する
		if(i !=0 && text[i].charCodeAt(0) != 32 && text[i-1].charCodeAt(0) != 32){
			convertedText += "-";
		}

		let decCh = text[i].charCodeAt(0);

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			decCh = decCh - 64;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			decCh = decCh - 96;
		}

		if(decCh == 32){//スペースの場合
			convertedText += " ";
		}else{
			convertedText += decCh;
		}
	}
	return convertedText;
}


/*
*	A1Z26で暗号化されたテキストを復号する
*
*	ハイフンで繋がっている数字を一つの単語として認識し、
*	スペースを単語の区切りとする
*	元の文章に関わらず、すべて大文字で復号される
*
* 	@param {string} text 平文
*
*	@return {string}
*/
function decodeA1z26(text){
	let convertedText = "";

	let asciiStr = text.split(" ");
	const asciiStrLen = asciiStr.length;
	for(let y=0; y<asciiStrLen; y++){
		let asciiCh = asciiStr[y].split("-");
		for(let i=0;i<asciiCh.length;i++){
			let decCh = parseInt(asciiCh[i]) + 64;
			convertedText += String.fromCharCode(decCh);
		}

		if(y != asciiStr.length - 1)//文章最後の単語でない場合、単語の区切りにスペースを加える
			convertedText += " ";
	}
	return convertedText;
}

/*
*	平文を二進数に変換する
*
*	ASCII文字を10進数に変換し、それを2進数に変換する
*	変換した1文字が8桁に満たない場合、8桁になるように先頭に"0"を追加する
*
* 	@param {string} text 平文
*
*	@return {string}
*/
function encodeBinary(text){
	let convertedText = "";
	
	const textLen = text.length;
	for(let i=0; i<textLen; i++){
		let binary = text[i].charCodeAt(0).toString(2);

		let zero = "";
		for(var y=0; y<8-binary.length; y++){
			zero += "0";
		}
		binary = zero + binary;
		
		convertedText += binary;
	}
	return convertedText;
}


/*
*	二進数で書かれたテキストをアルファベットへ変換する
*
*	2進数から10進数へ変換し、対応するASCII文字コードに変換する
*
* 	@param {string} text 平文
*
*	@return {string}
*/
function decodeBinary(text){
	let convertedText = "";

	const numOfChar = text.length / 8;
	for(let i=0; i<numOfChar; i++){
		let binCh = text.substr(0,8);
		text = text.substr(8);
		convertedText += String.fromCharCode(parseInt(binCh,2));
	}
	return convertedText;
}

//A~Zを0~25に変換して返す
function getNumberOfAlphabet(ch){
	let decCh = ch.charCodeAt(0);
	if(decCh >= 65 && decCh < 65 + 26){//大文字のとき
		decCh = decCh - 65;
	}else if(decCh >= 97 && decCh < 97 + 26){//小文字のとき
		decCh = decCh - 97;
	}
	return decCh;
}