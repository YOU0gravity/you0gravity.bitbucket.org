var gc = undefined;	//GameControllerクラスのインスタンスが入る

/*
*	インスタンスに、文章と場所・サイズなどの情報を持たせ、
*	メッセージウィンドウとしてエレメントを表示する
*
*	PlayerクラスとEnemyWindowクラスの親クラス
*
*	@param {String} kind
*/
class SentenceWindow {
	constructor(kind = ""){
		this.kind = kind;
		this.id = 0;

		this.sentenceJp = "";
		this.sentenceRm = "";

		this.posX = 0;
		this.posY = 0;
		this.div;
	}

	getName(){return this.kind + this.id;}


	/*
	*	文章情報
	*/
	getSentence(){
		let sentenceObject = new Object();
		sentenceObject.sentenceJp = this.sentenceJp;
		sentenceObject.sentenceRm = this.sentenceRm;
		return sentenceObject;
	}
	setSentenceObj(sentenceObj){this.setSentence(sentenceObj.sentenceJp, sentenceObj.sentenceRm);}
	setSentence(sentenceJp, sentenceRm){
		this.sentenceJp = sentenceJp;
		this.sentenceRm = sentenceRm;

		//更新したプロパティをエレメントに反映させる
		this.div.innerHTML = this.ELEMENT_TEMPLATE(this.sentenceJp, this.sentenceRm);
	}
	ELEMENT_TEMPLATE(sentenceJp, sentenceRm){
		const string = `
			<div class="sentence-jp">${sentenceJp}</div>
			<div class="sentence-rm">${sentenceRm}</div>`
		return string;
	}


	/*
	*	エレメント
	*/
	setPos(posX = -1, posY = -1){
		if(posX != -1){
			this.posX = posX;
			this.div.style.left = this.posX + "px";
		}
		if(posY != -1){
			this.posY = posY;
			this.div.style.top = this.posY + "px";
		}
	}
	setPosObj(posObj){this.setPos(posObj.posX, posObj.posY);}
	getPos(){
		let posObject = new Object();
		posObject.posX = this.posX;
		posObject.posY = this.posY;
		return posObject;
	}

	setSize(width = 200, height = 100, fontSize = 16){
		this.div.style.width = width + "px";
		this.div.style.height = height + "px";
		this.div.style.fontSize = fontSize + "px";
	}

	setVisible(isVisible = false){
		if(isVisible){
			this.div.style.display = "block";
		}else{
			this.div.style.display = "none";
		}
	}

	setDestructionTime(time){
		if(time <= 0){
			this.div.parentNode.removeChild(this.div);
			this.div.innerHTML = "";
		}else{
			setTimeout(function(){
				this.div.parentNode.removeChild(this.div);
				this.div.innerHTML = "";
			}.bind(this), time);
		}
	}

	createDiv(divId, sentenceObj){
		const div = document.getElementById(this.kind + divId);

		if (!div) {
			this.id = divId;
			this.sentenceJp = sentenceObj.sentenceJp;
			this.sentenceRm = sentenceObj.sentenceRm;

			let container = document.createElement('div');
			container.innerHTML = `
				<div class="refreshable">
					${this.ELEMENT_TEMPLATE(this.sentenceJp, this.sentenceRm)}
			    </div>`;
			container.setAttribute('class', "sentence-window");
			container.setAttribute('id', this.kind + this.id);

			document.getElementById("graphic-area").appendChild(container);

			this.div = container;

			return this.div;
			
		}else{
			this.createDiv(divId + 1, sentenceObj);
		}
	}
}


class Player {
	constructor(kind){
		this.kind = kind;
		this.createDiv(0);

		this.posX = (window.innerWidth - 200) / 2
		this.posY = window.innerHeight + 400;
		this.div.style.top = this.posY + "px";
		this.div.style.left = this.posX + "px";

		this.setPosX((window.innerWidth - 200) / 2);

		this.targetEnemyName = "";
		this.ATTACK_INTERVAL = 2500;

		if(kind == "buddy"){
			this.SEARCH_INTERVAL = 1500;
			setTimeout(function(){
				this.autoSearching();
			}.bind(this), 1000);
		}
	}

	/*
	*	プレイヤーエレメント
	*/
	setPosObj(posObj){this.setPosX(posObj.posX);}
	setPosX(posX){
		if(this.posX != posX){
			this.posX = posX;

			this.div.animate(
				[
					{ transform: 'scale(1.2, 1)' },
					{ transform: 'scale(0.1, 1)' },
				    { transform: 'scale(1, 1)' },
				],{
					duration: 500,
					iterations: 1
				}
			);
		}

		this.posY = window.innerHeight / 10 * 7;
		$("#"+this.kind + this.id).animate({
			'top': this.posY + 'px',
			'left': this.posX + 'px'
		});
	}
	
	createDiv(divId){
		const div = document.getElementById(this.kind + divId);

		if (!div) {
			this.id = divId;

			let container = document.createElement('div');
			container.innerHTML =  `${createPlayerDiv(this.kind + this.id)}`;
			container.setAttribute('class', "players-div");
			container.setAttribute('id', this.kind + this.id);
			document.getElementById("graphic-area").appendChild(container);

			this.div = container;
		}else{
			this.createDiv(divId + 1);
		}
	}


	/*
	*	照準・弾などのエレメント
	*/
	setTarget(targetEnemyName = ""){
		if(targetEnemyName == ""){
			this.targetEnemyName = "";
			let addDiv = document.getElementById("aim" + this.id);
			addDiv.parentNode.removeChild(addDiv);
			return;
		}

		this.targetEnemyName = targetEnemyName;
		this.animateAttacking();
		
	}

	createAimScope(targetName, playerId){
		let container = document.createElement('div');

		container.innerHTML = createAimDiv(playerId);

		container.setAttribute('class', "aim");
		container.setAttribute('id', "aim" + playerId);

		const targetDiv = document.getElementById(targetName);
		targetDiv.insertBefore(container, targetDiv.firstChild);
	}

	shootBullet(targetEnemyName){
		new Bullet(this.posX, window.innerHeight / 10 * 7, this.id, targetEnemyName);
	}

	inflictDamageToTargetEnemy(damageValue = 100){
		const targetNum = gc.pickUpEnemyNumByName(this.targetEnemyName);

		if(targetNum == -1) return false;

		//isDeadメソッドでダメージを与え、HPがゼロになったら倒す
		if(gc.enemiesArray[targetNum].isDead(damageValue)){
			gc.destroyEnemy(this.targetEnemyName);
			this.targetEnemyName = "";
			return true;
		}
		return false;
	}


	/*
	*	味方プレイヤーAI
	*/
	autoSearching(){
		if(this.targetEnemyName)
			return;

		//buddyが複数いる場合、なるべくターゲットがかぶらないようにする
		let targetNum = this.id;
		const enemyLen = gc.enemiesArray.length;

		if(targetNum >= enemyLen)
			targetNum = enemyLen - 1;

		if(enemyLen > 0 && gc.enemiesArray[targetNum]){
			let targetEnemyName = gc.enemiesArray[targetNum].getName();
			
			//プレイヤーがターゲットにしている敵は、なるべく狙わないようにする
			if(targetEnemyName == gc.currentEnemyName && targetNum < enemyLen - 1)
				targetEnemyName = gc.enemiesArray[targetNum++].getName();
			
			this.targetEnemyName = targetEnemyName;

		}else{
			console.log("敵が見当たらないので、" + this.SEARCH_INTERVAL/1000 + "秒後にもう一度探します");
		}

		//ターゲットの有無で、攻撃などの処理を行い、
		//一定時間後に自分を再帰的に呼び出す
		if(gc.isPlaying){
			let delay = 0;
			if(this.targetEnemyName){
				this.animateAttacking();

				setTimeout(function(){
					this.inflictDamageToTargetEnemy();
					this.targetEnemyName = "";
				}.bind(this), this.ATTACK_INTERVAL);

				delay = this.ATTACK_INTERVAL;
			}else{
				clearTimeout(this.bulletTimeoutID);
			}

			setTimeout(function(){
				this.autoSearching();
			}.bind(this), this.SEARCH_INTERVAL + delay);//攻撃を行う場合、再帰呼び出しをATTACK_INTERVALミリ秒遅らせる
		}
	}
	animateAttacking(){
		const targetNum = gc.pickUpEnemyNumByName(this.targetEnemyName);
		if(targetNum == -1){
			this.targetEnemyName = "";
			return;
		}

		const targetEnemyPosX = gc.enemiesArray[targetNum].getPos().posX;

		this.setPosX(targetEnemyPosX);
		this.createAimScope(this.targetEnemyName, this.id + 1);
		this.bulletTimeoutID = setTimeout(function(){
			this.autoAddBulletEffect();
		}.bind(this), 1500);
	}
	autoAddBulletEffect(){
		if(gc.pickUpEnemyNumByName(this.targetEnemyName) == -1)
			return;

		console.log("オートで弾を撃っています " + this.id + ": " + this.targetEnemyName);
		new Bullet(this.posX, window.innerHeight / 10 * 7, this.id, this.targetEnemyName);

		this.bulletTimeoutID = setTimeout(function(){
			this.autoAddBulletEffect();
		}.bind(this), this.ATTACK_INTERVAL/3);
	}
}

/*
*	敵表示に関するクラス
*
*	新たに自分のDivを作成し、独自のIDを付与する関数、
*	HP、攻撃頻度、攻撃力などのパラメーターを操作する関数がある
*/
class EnemyWindow extends SentenceWindow {
	//Override
	constructor(posX, sentenceObj, enemyHp = 100, speed = 30, attackPower = 25, enemyCount = -1, kind = "enemy"){
		super(kind);
		this.enemyHp = enemyHp;
		this.speed = speed;
		this.attackPower = attackPower;
		this.isTypable = false;

		this.SPAWN_TOP_OFFSET = 320;
		this.animation;

		this.createDiv(enemyCount, sentenceObj);	//Divを作り、固有のIDを付与する
		super.setPos(posX, -this.SPAWN_TOP_OFFSET);//createDivでエレメントを作り出した後で呼び出す

		this.BIRTH_TIME = new Date().getTime();
		this.approach(this.speed);
	}

	/*
	*	文章情報
	*/
	setSentenceObj(sentenceObj, isTypable = false){this.setSentence(sentenceObj.sentenceJp, sentenceObj.sentenceRm, isTypable);}
	setSentence(sentenceJp, sentenceRm, isTypable = false){
		this.sentenceJp = sentenceJp;
		this.sentenceRm = sentenceRm;
		this.isTypable = isTypable;
		this.refresh();
	}

	isDead(damage = 0){		
		this.enemyHp -= damage;

		if(this.enemyHp <= 0){
			return true;
		}

		//小数点第2位以下の数値を切り捨て
		this.enemyHp = floatFormat(this.enemyHp, 2);
		this.refresh();

		return false;
	}

	/*
	*	エレメント
	*/
	createDiv(divId, sentenceObj){
		//指定したIDのエレメントが存在するか確認し、存在しなければ新たに作り出す
		const div = document.getElementById(this.kind + divId);

		if (!div) {
			this.id = divId;
			this.sentenceJp = sentenceObj.sentenceJp;
			this.sentenceRm = sentenceObj.sentenceRm;

			let container = document.createElement('div');
			let enemyTemplate = ``;

			switch(this.kind){
				case "enemy":
				/*FALLTHROUGH*/
				case "buggerfly":
					enemyTemplate = `${createBuggerflyDiv(this.kind + this.id)}`;
					break;
				default:
					break;
			}

			if(!enemyTemplate)
				return;

			container.innerHTML = `
				${enemyTemplate}
				<div class="refreshable">
					${this.ELEMENT_TEMPLATE(this.sentenceJp, this.sentenceRm, this.isTypable)}
			    </div>`;


			container.setAttribute('class', "enemy-div");
			container.setAttribute('id', this.kind + this.id);

			document.getElementById("graphic-area").appendChild(container);

			this.div = container;

		}else{
			//指定した名前のIDが既に存在する場合、数字を繰り上げ再帰的に呼び出す
			this.createDiv(divId + 1, sentenceObj);
		}

	}

	changeTypableOrNot(isTypable){
		this.setSentence(this.sentenceJp, this.sentenceRm, isTypable);
	}

	ELEMENT_TEMPLATE(sentenceJp, sentenceRm, isTypable = false){
		const NEW_LINE_INSERTING_NUM = 15;
		let string = "";

		if(isTypable){
			string = `
			    <div id="illustrative-area-jp" class="sentence-jp">${sentenceJp}</div>
			    <div class="sentence-rm" id="input-animation-area">
					<div id="done-input" class="outputs"></div>
					<div id="not-done-input" class="outputs">${sentenceRm}</div>
				</div>`;
		}else{
			if(sentenceRm / NEW_LINE_INSERTING_NUM > 1)
				sentenceRm = insertTagAtRegularIntervals(sentenceRm, "<br>", NEW_LINE_INSERTING_NUM);
			
			string = `
			    <div class="sentence-jp">${sentenceJp}</div>
			    <div class="sentence-rm">${sentenceRm}</div>`;
		}

	    return string;
	}

	refresh(){
		this.div.lastChild.innerHTML = this.ELEMENT_TEMPLATE(this.sentenceJp, this.sentenceRm, this.isTypable);
	}

	beingDestroyedEffect(){
		this.div.innerHTML = createDestroyedDiv();
		let top = removePx(this.div.style.top) + 50;
		this.div.style.top = top + "px";
	}

	approach(durationSecond = 10){
		this.durationMs = durationSecond * 1000;
		const name = this.getName();
		const damageValue = this.attackPower;
		const moveValue = window.innerHeight + this.SPAWN_TOP_OFFSET;

		this.animation = this.div.animate(
			[
				{ transform: 'translateY(0)' },
			    { transform: 'translateY('+ moveValue +'px)' },
			],{
				duration: durationSecond * 1000,
				iterations: 1
			}
		);
		//倒されずに生き残った場合、自壊する
		this.animation.onfinish = function(){
			if(gc.getEnemyHpByName(name) > 0){
				gc.destroyEnemy(name, true);
			}
		}
	}

	//WebAnimationsは、エレメントの実際の位置を変えずにアニメーションさせているので、
	//移動速度と経過時間から計算して、エレメントのYの位置を導き出す
	computeCalculatedPosY(additionalTime = 0){
		const grossMoveValue = window.innerHeight + this.SPAWN_TOP_OFFSET;
		const livingTimeMs = new Date().getTime() - this.BIRTH_TIME + additionalTime;
		const alreadyMovedValue = computeCurrentMoveValue(grossMoveValue, this.durationMs, livingTimeMs);
		return grossMoveValue - alreadyMovedValue;
	}
}

class Bullet {
	constructor(posX = -1, posY = -1, playerId, targetEnemyName){
		const enemyNum = gc.pickUpEnemyNumByName(targetEnemyName);
		if(enemyNum == -1)
			return;

		this.posX = posX;
		this.posY = posY;
		this.id = playerId;
		this.div = "";
		this.targetEnemyName = targetEnemyName;

		this.createDiv(this.id);
		this.moveBullet();
		this.animateBullet(enemyNum);
	}
	createDiv(id){
		let container = document.createElement('div');

		container.innerHTML = createBulletDiv();

		container.setAttribute('class', "bullet");
		container.setAttribute('id', "bullet" + this.id);

		document.getElementById("graphic-area").appendChild(container);

		this.div = container;
	}
	moveBullet(){
		if(this.posX == -1 || this.posY == -1){
			return false;
		}else{
			this.div.style.top = this.posY + "px";
			this.div.style.left = this.posX + "px";
			return true;
		}
	}

	//常に同じ速度になるように、距離と時間を計算して、アニメーションする
	//また、敵の位置で消えるようにする
	animateBullet(enemyNum){
		if(this.posX == -1 || this.posY == -1 || enemyNum == -1){
			return false;
		}else{
			const SPEED = 3;/* px/ms */
			const destinationY = gc.enemiesArray[enemyNum].computeCalculatedPosY();
			const moveValue = destinationY + /*const OFFSET = */ - 300;
			const durationMs = moveValue / SPEED;

			if(moveValue < 0 || durationMs < 0){
				let div = document.getElementById("bullet" + this.id);
				div.parentNode.removeChild(div);
				return false;
			}
			
			this.animation = this.div.animate(
				[
					{ transform: 'translateY(0)' },
				    { transform: 'translateY(-'+ moveValue +'px)' },
				],{
					duration: durationMs,
					iterations: 1
				}
			);

			this.animation.onfinish = function(){
				let div = document.getElementById("bullet" + this.id);
				div.parentNode.removeChild(div);
			}.bind(this);

		}
	}
}


/*
*	ゲームの主要な機能を操作するクラス
*
*	問題文を専用のエリアで初期化したり、敵を出現させたりする
*	一つしか存在しないようにする
*/
class GameController {
	constructor(){
		//よく使うDivエレメントのショートカットを作成
		this.currentCharArea = document.getElementById("current-char-area");
		this.inputArea = document.getElementById("input-area");
		this.illustrativeAreaJp = document.getElementById("illustrative-area-jp");
		this.comboArea = document.getElementById("combo-area");
		this.graphicArea = document.getElementById("graphic-area");
		this.doneInput = document.getElementById("done-input");
		this.notDoneInput = document.getElementById("not-done-input");

		//audio
		this.audioSwitch = [document.getElementById("audio-switch1"), document.getElementById("audio-switch2"), document.getElementById("audio-switch3")];
		this.audioDamage = document.getElementById("audio-damage");
		this.audioSwitchCount = 0;	

		this.initializeAllState();

		//外部ファイルの読み込み
		this.stageIterator = STAGE_INFORMATION[Symbol.iterator]();//../oneLine/stage.js

		//コマンドキーの設定
		this.CONFIG = {
			TARGET_REMOVING_KEY: " ",
			WAVE_SKIP_KEY: "/",
			GAME_START_KEY: " ",
		}

		//デバッグモード
		this.debugMode = false;
		if(this.debugMode){
			this.debugMode = new DebugMode();
		}

		//画面をクリックすればフォームへフォーカスする
		this.graphicArea.addEventListener("click", function(){
			this.inputArea.focus();
		}.bind(this));

		//マルチモード用(現在使用していない)
		this.players = [];
		this.myPlayerId = 0;


		//難易度選択画面の表示
		this.changeState(0);

		console.log("GameControllerの初期化が完了しました");
	}

	initializeAllState(){
		this.difficulty = "Menu";
		this.isPlaying = false;
		this.currentState = 0;

		this.initializeTypeState();
		this.initializeEnemyState();
	}

	initializeEnemyState(){
		this.isWaitingForExtermination = false;
		this.enemiesArray = [];
		this.spawnTimeout = "";
		this.currentEnemyName = "";
		this.enemyCount = 0;
		this.waveSentenceCount = 0;
		this.totalDestroyedEnemies = 0;
	}

	initializeTypeState(){
		this.workingSentence = "";
		this.sentenceCount = 0;
		this.combo = 0;
		this.streakMissType = 0;
		this.totalTypedChars = 0;
		this.totalMissType = 0;
	}

	getEnemyHpByName(enemyName){
		const designatedNum = this.pickUpEnemyNumByName(enemyName);
		if(designatedNum == -1){
			return -1;
		}else{
			return this.enemiesArray[designatedNum].enemyHp;
		}
	}



	/*
	*	ステージ情報を格納したファイルをの中身を、一行ずつ処理する
	*/
	readlineFromStageData(){
		if(!this.isPlaying)
			return;
		
		const oneLine = this.stageIterator.next();

		if(!oneLine.done){
			let kind = oneLine.value[0];
			let doesPause = false;
			let pauseTime = 3000;

			/*
			*	コマンド内に"@"が存在するとき、本来の処理の後、指定したミリ秒次の処理を待つ
			*/
			if(kind.match("@")){
				const index = kind.indexOf("@");
				kind = kind.replace("@", "");
				doesPause = true;
			}

			switch(kind){
				/*
				*	ブラウザのコンソールにメッセージを出力する
				*	ex.
				*	["console", メッセージの内容{String}]
				*/
				case "log": case "console":
					const consoleMessage = oneLine.value[1];
					console.log("[" + consoleMessage + "]");
					break;

				/*
				*	SentenceWindowクラスを用いたメッセージウィンドウを出現させる
				*	ex.
				*	["message", メッセージの内容{String}, 表示時間(ミリ秒){Number}, 大きさ{String}]
				*
				*	大きさは"small", "middle", "big"の三種類を指定できる
				*/
				case "msg": case "message":
					this.gameStartWindow = this.createMessageWindow(oneLine.value[1], oneLine.value[2], oneLine.value[3]);
					if(doesPause && oneLine.value[2])
						pauseTime = oneLine.value[2];
					break;

				/*
				*	指定したミリ秒数、次の処理を待つ
				*	ex.
				*	["time", durationS{Number}]
				*/
				case "pause": case "time": case "wait":
					let tempVal = oneLine.value[1];
					if(tempVal == "extermination" || tempVal == "ex"){
						this.isWaitingForExtermination = true;
						return;
					}
					doesPause = true;
					pauseTime = oneLine.value[1] || 3;
					break;

				/*
				*	EnemyWindowクラスを用いて、敵を出現させる
				*	ex.
				*	(省略)["enemy", ブロック{String}]
				*	(省略)["enemy@", ブロック{String}, pauseTimeS{Number}]
				*	(正式)["enemy@", ブロック{String}, pauseTimeS{Number}, hp{Number}, durationS{Number}, attackPower{Number}, sentenceObj{Object}]
				*
				*	ブロックは"b"+numの形で表され、"b0"から"b4"まである
				*/
				case "enemy": case "buggerfly"://種類が増えたら、ここに書き加える
					this.spawnOneEnemy(kind, oneLine.value[1], oneLine.value[3], oneLine.value[4], oneLine.value[5], oneLine.value[6]);
					if(doesPause && oneLine.value[2])
						pauseTime = oneLine.value[2];
					break;

				case "result":
					this.players[this.myPlayerId].setPosX(window.innerWidth - 200);
					let resultStr = this.generateEvalStr();
					this.isPlaying = false;
					this.gameStartWindow = this.createMessageWindow(resultStr, "Infinity", "small", 20);
					if(doesPause && oneLine.value[2])
						pauseTime = oneLine.value[2];
					break;

			}

			if(doesPause){
				let pauseTimeMs = pauseTime * 1000;
				this.spawnTimeout = setTimeout("gc.readlineFromStageData()", pauseTimeMs);
			}else{
				this.readlineFromStageData();
			}

		}
	}

	generateEvalStr(){
		//敵の持つ文章の文字より、実際に打った文字の方が数が大きくなる時がある(ex.siをshiに変換したとき)
		//TODO: ちゃんと調整する
		if(this.totalTypedChars > this.waveSentenceCount)
			this.waveSentenceCount = this.totalTypedChars;

		let roundedRate = Math.round(((this.totalTypedChars - (this.totalMissType/3)) / this.waveSentenceCount) * 10);
		let resultStr = `
			難易度　　: ${this.difficulty}<br>
			倒した敵　: ${this.totalDestroyedEnemies}/${this.enemyCount}体<br>
			タイプ数　: ${this.totalTypedChars}/${this.waveSentenceCount}打<br>
			ミス数　　: ${this.totalMissType}打<br>
			<br>
		`;

		let evalText = "";
		switch(roundedRate){
			case 10:
				if(this.difficulty == "Hard")
					evalText = "＜幻のタイピスト＞<br>これが最高評価！<br>ありえない速さ！";
				if(this.difficulty == "Normal")
					evalText = "＜凄腕タイピスト＞<br>この難易度はもう完璧！<br>Hardにも挑戦してみよう！";
				if(this.difficulty == "Easy")
					evalText = "＜上級タイピスト＞<br>この難易度はもう完璧！<br>Normalにも挑戦してみよう！";
			break;
			case 9:
				if(this.difficulty == "Hard")
					evalText = "＜伝説のタイピスト＞<br>すごすぎる・・・！！<br>これぞまさに伝説！";
				if(this.difficulty == "Normal")
					evalText = "＜凄腕タイピスト＞<br>良いタイピング速度！<br>Hardにも挑戦してみよう！";
				if(this.difficulty == "Easy")
					evalText = "＜上級タイピスト＞<br>良いタイピング速度！<br>Normalにも挑戦してみよう！";
			break;
			case 8:
				if(this.difficulty == "Hard")
					evalText = "＜マッハタイピスト＞<br>音速を超えるタイピング速度！";
				if(this.difficulty == "Normal")
					evalText = "＜上級タイピスト＞<br>とってもいい感じ！";
				if(this.difficulty == "Easy")
					evalText = "＜中級タイピスト＞<br>いい感じ！";
			break;
			case 7:
				if(this.difficulty == "Hard")
					evalText = "＜凄腕タイピスト＞<br>とってもいい感じ！";
				if(this.difficulty == "Normal")
					evalText = "＜上級タイピスト＞<br>とってもいい感じ！";
				if(this.difficulty == "Easy")
					evalText = "＜中級タイピスト＞<br>いい感じ！";
			break;
			case 6:
				if(this.difficulty == "Hard")
					evalText = "＜上級タイピスト＞<br>とってもいい感じ！";
				if(this.difficulty == "Normal")
					evalText = "＜中級タイピスト＞<br>いい感じ！";
				if(this.difficulty == "Easy")
					evalText = "＜初級タイピスト＞<br>いい感じ！";
			break;
			case 5:
				if(this.difficulty == "Hard")
					evalText = "＜上級タイピスト＞<br>いい感じ！";
				if(this.difficulty == "Normal")
					evalText = "＜中級タイピスト＞<br>いい感じ！";
				if(this.difficulty == "Easy")
					evalText = "＜初級タイピスト＞<br>焦らず、一体ずつ倒していこう！";
			break;
			case 4: case 3:
				if(this.difficulty == "Hard")
					evalText = "＜中級タイピスト＞<br>難しすぎると感じたら、<br>一つ前の難易度でチャレンジしてみよう！";
				if(this.difficulty == "Normal")
					evalText = "＜初級タイピスト＞<br>難しすぎると感じたら、<br>一つ前の難易度でチャレンジしてみよう！";
				if(this.difficulty == "Easy")
					evalText = "＜初級タイピスト＞<br>焦らず、一体ずつ倒していこう！";
			break;
			case 2: case 1: case 0:
				if(this.difficulty == "Hard")
					evalText = "＜中級タイピスト＞<br>難しすぎると感じたら、<br>一つ前の難易度でチャレンジしてみよう！";
				if(this.difficulty == "Normal")
					evalText = "＜初級タイピスト＞<br>難しすぎると感じたら、<br>一つ前の難易度でチャレンジしてみよう！";
				if(this.difficulty == "Easy")
					evalText = "＜初級タイピスト＞<br>これからコツコツ頑張っていこう！";
			break;
		}
		resultStr += "評価　　　: " + evalText + "<br><br>[1]: メニューへもどる";

		return resultStr;
	}

	/*
	*	SentenceWindowクラスのインスタンスを作成するための様々なプロパティを用意する
	*/
	createMessageWindow(msg, destructionTime = "Infinity", size = "middle", lineCharLimit = 10){
		msg = insertTagAtRegularIntervals(msg, "<br>", lineCharLimit);
		const lineCount = countSearchingWord(msg, "<br>");

		let msgWin = new SentenceWindow("message");
		msgWin.createDiv(0, {sentenceJp: msg, sentenceRm: ""});

		let windowHeight = 0;
		let windowWidth = 0;
		let windowFontSize = 0;

		//二行を想定
		switch(size){
			case "small":	windowHeight = 68; windowWidth = 50; windowFontSize = 45; break;
			case "middle":	windowHeight = 102; windowWidth = 70; windowFontSize = 67; break;
			case "big":		windowHeight = 138; windowWidth = 95; windowFontSize = 90; break;
			default:
				if(typeof(size) == "object" && size.length == 3){
					windowHeight = size[0]; windowWidth = size[1] / 100; windowFontSize = size[2];
				}
				break;
		}
		windowHeight *= lineCount;
		windowWidth *= lineCharLimit;
		
		msgWin.setSize(windowWidth, windowHeight, windowFontSize);
		msgWin.setPos((window.innerWidth - windowWidth)/2, (window.innerHeight - windowHeight)/2);

		if(destructionTime != "Infinity")
			msgWin.setDestructionTime(destructionTime * 1000);

		return msgWin;
	}

	/*
	*	EnemyWindowクラスのインスタンスを作成するための様々なプロパティを用意する
	*/
	spawnOneEnemy(kind = "enemy", posBlock = 0, hp = 50, durationS = 30, power = 25, sentenceObj = {}){
		if(this.enemiesArray.length >= this.existingInitialArray.length - 1){
			console.log("これ以上敵を召喚できません");
			return false;
		}

		if(!(sentenceObj.sentenceJp && sentenceObj.sentenceRm)){
			sentenceObj = this.pickSentenceRandomly();
		}else{
			//敵が指定した文章を持っている場合
			const [first] = sentenceObj.sentenceRm;

			//既に同じ頭文字を持つ敵が存在する場合、それを倒した扱いにする
			if(checkIfTheInitialAlreadyExists(this.existingInitialArray, first)){
				const enemyNum = this.pickUpEnemyNumByInitial(first);
				const enemyName = this.pickUpEnemyNameByNum(enemyNum);
				if(enemyName)
					this.destroyEnemy(enemyName);
			}

			const designatedNum = convertAlphabetCharToDecimalNumber(first);
			this.existingInitialArray[designatedNum] = true;
		}

		if(!sentenceObj.sentenceJp){
			console.log("問題文が空なので、敵は生成されませんでした");
			return false;
		}

		//posBlockの値(0~4)によって、出現させる具体的な位置を決める
		const ENEMY_SIZE = 200
		let posX = 0;
		switch(posBlock){
			case "b0":
				posX = 0;
				break;
			case "b1":
				posX = (window.innerWidth - ENEMY_SIZE) / 4;
				break;
			case "b2":
				posX = (window.innerWidth - ENEMY_SIZE) / 2;
				break;
			case "b3":
				posX = (window.innerWidth - ENEMY_SIZE) * 3 / 4;
				break;
			case "b4":
				posX = window.innerWidth - ENEMY_SIZE;
				break;
			default:
				if(typeof posBlock == typeof 0)
					posX = posBlock;
				break;
		}

		let enemy = new EnemyWindow(posX, sentenceObj, hp, durationS, power, this.enemyCount, kind);
		this.enemiesArray.push(enemy);

		this.enemyCount++;
		this.waveSentenceCount += sentenceObj.sentenceRm.length;
		// console.log(enemy.getName() + "が出現しました");

		return enemy;
	}

	/*
	*	問題の文章を専用のエリアへセットし、sentenceCountを0に（初期化）する
	*
	*	@param {Object} sentenceObj	sentenceJp,sentenceRmというプロパティを持ち、
	*								前者は日本語の文章、後者はそれをローマ字の文章をString型で保持している
	*
	*/
	setTryingSentence(sentenceObj){
		const typableEnemyNum = this.pickUpEnemyNumByName(this.currentEnemyName);
		this.enemiesArray[typableEnemyNum].setSentenceObj(sentenceObj, true);

		//新たに作り直されるので、ショートカットを設定し直す
		this.illustrativeAreaJp = document.getElementById("illustrative-area-jp");
		this.doneInput = document.getElementById("done-input");
		this.notDoneInput = document.getElementById("not-done-input");

		this.workingSentence = sentenceObj.sentenceRm;
		this.sentenceCount = 0;

		//入力済みのエリアを空にし、未入力のエリアに問題の文章をセット
		this.illustrativeAreaJp.innerHTML = sentenceObj.sentenceJp;
		this.doneInput.innerHTML = "";
		this.notDoneInput.innerHTML = this.workingSentence;
	}


	/*
	*	問題配列から、場に存在しない頭文字をもつ文章をランダムで選び出し、それを返す
	*	頭文字配列の、該当する頭文字の領域へtrueを代入する
	*	@return {Object}
	*/
	pickSentenceRandomly(){
		let sentenceObj = new Object();
		let limit = 30;
		let isSentenceObjectFound = false;

		//場に存在しない頭文字をランダムに選び出し、その頭文字を持つ文章をランダムに選び出して返す
		while(!isSentenceObjectFound && limit > 0){
			const designatedNum = pickNumberOfNotExistInitial(this.questionArray, this.existingInitialArray);

			if(designatedNum != -1){
				sentenceObj = pickSentenceHavingDesignatedInitial(this.questionArray, designatedNum);

				if(sentenceObj.sentenceJp && sentenceObj.sentenceRm){
					this.existingInitialArray[designatedNum] = true;
					isSentenceObjectFound = true;
				}
			}

			limit--;
		}

		if(isSentenceObjectFound){
			// console.log("問題文'" + sentenceObj.sentenceJp + "'を選び出しました");
		}else{
			console.log("場に存在しない頭文字をもつ文章オブジェクトは見つかりませんでした");
		}

		return sentenceObj;
	}

	/*
	*	<重要>
	*	1文字入力されるたびに呼び出される、このゲームの中で一番大事な関数。
	*	入力された文字を元に、敵をターゲットし、
	*	その敵の持っている文章情報と入力された文字を比較、
	*	同じであれば、正解として文字数のカウントを1つ進める。
	*
	*	敵の持っている文章の文字をすべて打ち終えた場合、
	*	敵にダメージを与える。
	*/
	onTyped(inputValue = ""){
		if(!inputValue)
			inputValue = this.inputArea.value;

		this.inputArea.value = "";

		if(this.processCommand(inputValue))
			return;
		
		//ターゲットしている敵がいない場合、探す
		if(!this.currentEnemyName){
			const enemyNum = this.pickUpEnemyNumByInitial(inputValue);
			const didChangeTarget = this.changeTarget(enemyNum);

			//入力した頭文字の敵が見つからなかった場合、代わりの文字を探す
			if(!didChangeTarget){

				const searchingInitial = this.canUseAlterInitial(inputValue);

				if(!searchingInitial)
					return;

				const enemyNum = this.pickUpEnemyNumByInitial(searchingInitial);
				this.changeTarget(enemyNum);
			}

			this.players[this.myPlayerId].createAimScope(this.currentEnemyName, 0);

			// if(this.multiMode){
			// 	this.multiMode.requestTarget(this.myPlayerId, this.currentEnemyName);
			// }
		}

		//ターゲットしている敵が存在する場合、処理を継続する
		if(this.currentEnemyName){
			//タイプされた文字が、入力すべき文字と同じかどうかをBooleanで格納
			const isSame = isSameChar(this.workingSentence, this.sentenceCount, inputValue);

			if(!isSame){
				//他の文字と変換可能かどうかをBooleanで格納 ex."じゃ"の子音は、"j"でも"zy"でも可なので、変換可能
				const canAlter = canTypeAlterChar(this.workingSentence, this.sentenceCount, inputValue);

				if(canAlter){
					//文字を置き換え可能だった場合、問題の文章を書き換える
					this.workingSentence = convertCharToAlter(this.workingSentence, this.sentenceCount, inputValue);
				}else{
					//文字を置き換え不可能だった場合、不正解時の処理を行う
					this.combo = 0;
					this.streakMissType++;
					this.totalMissType++;

					if(this.streakMissType % 6 == 0){
						this.gameStartWindow = this.createMessageWindow("他の敵を狙いたい場合は、<br>スペースを押してね", 2, "small", 13);
					}
					return;
				}
			}

			/*
			*	以下、入力した文字が、入力すべき文字と同じであった場合(正解時)
			*/
			this.streakMissType = 0;

			if(this.isPlaying){
				this.combo++;
				this.totalTypedChars++;
			}

			this.sentenceCount++;
			const sentenceWithBr = insertTagAtRegularIntervals(this.workingSentence, "<br>", 10);
			const sentenceCountWithBr = computeCountWithStrInsertedAtRegularIntervals(this.sentenceCount, "<br>", 10);

			this.doneInput.innerHTML = sentenceWithBr.substr(0,sentenceCountWithBr);
			this.notDoneInput.innerHTML = sentenceWithBr.substr(sentenceCountWithBr);

			const tempSentenceCount = this.sentenceCount;
			if(tempSentenceCount > 0 && tempSentenceCount % 5 == 0){
				this.players[this.myPlayerId].shootBullet(this.currentEnemyName);
			}

			switch(this.workingSentence[this.sentenceCount]){
				case undefined://問題の文章を全てタイプし終えている場合
					const damageValue = computeDamage(100, this.combo, undefined);
					const enemyNum = this.pickUpEnemyNumByName(this.currentEnemyName);
					const isDead = this.enemiesArray[enemyNum].isDead(damageValue);

					// if(this.multiMode){
					// 	this.multiMode.requestAttack(this.myPlayerId, damageValue);
					// }

					if(this.audioDamage)
						this.audioDamage.play();

					if(!isDead){
						this.removeInitialFromArray(enemyNum);

						//まだターゲットが生きている場合、新たに文章をセットする
						this.enemiesArray[enemyNum].setSentenceObj(this.pickSentenceRandomly());
						this.currentEnemyName = "";

					}else{
						this.destroyEnemy(this.currentEnemyName);
					}
					break;

				default://まだ問題の文章をタイプしきっていない場合
					this.currentCharArea.innerHTML = this.workingSentence[this.sentenceCount];
					break;
			}
			return;

		}else{
			console.log("敵がいません!");
			return;
		}
	}

	//変換可能ならその文字を返し、不可能なら空文字を返す
	canUseAlterInitial(replacing){
		let charAfterReplaced = "";

		switch(replacing){
			case 'z' : charAfterReplaced = 'j'; break;
			case 'j' : charAfterReplaced = 'z'; break;
			case 'f' : charAfterReplaced = 'h'; break;
			case 'h' : charAfterReplaced = 'f'; break;
			case 'q' : charAfterReplaced = 'k'; break;
			default: return ""; break;
		}

		if(checkIfTheInitialAlreadyExists(this.existingInitialArray, charAfterReplaced)){
			const tempEnemyNum = this.pickUpEnemyNumByInitial(charAfterReplaced);

			if(this.enemiesArray[tempEnemyNum]){
				const tempSentenceObj = this.enemiesArray[tempEnemyNum].getSentence();
				const canAlter = canTypeAlterChar(tempSentenceObj.sentenceRm, 0, replacing);

				if(canAlter)
					return charAfterReplaced;
			}

		}
		return "";
	}

	processCommand(inputValue){
		//コマンド関連

		if(!this.isPlaying){
			switch(this.currentState){
				case 0://難易度選択画面
					switch(inputValue){
						case "1":case "2":case "3":
							this.changeState(1, inputValue);
							break;
						case "4":
							this.changeState(2);
							break;
					}
					break;

				case 1://ゲームスタート確認画面
					if(inputValue == this.CONFIG.GAME_START_KEY){
						this.changeState(10);
					}else if(inputValue == "0"){
						this.changeState(0);
					}
					break;

				case 2://遊び方説明画面
					if(inputValue == "0"){
						this.changeState(0);
					}else if(inputValue == "1"){
						this.changeState(3);
					}

					break;
				case 3:
					if(inputValue == "0")
						this.changeState(0);
					break;

				case 10://ステージクリア後
					if(inputValue == "1"){
						this.initializeAllState();
						this.changeState(0);
					}
					break;
			}
		}

		if(this.debugMode){
			const isCalledDebugFunction = this.debugMode.callDebugFunction(inputValue);
			if(isCalledDebugFunction)
				return true;
		}

		// if(inputValue == this.CONFIG.WAVE_SKIP_KEY){
		// 	clearTimeout(this.spawnTimeout);
		// 	this.readlineFromStageData();
		// 	console.log("次のWAVEへ移ります");
		// }

		if(inputValue == this.CONFIG.TARGET_REMOVING_KEY){
			if(this.currentEnemyName){
				const targetNum = this.pickUpEnemyNumByName(this.currentEnemyName);
				this.enemiesArray[targetNum].changeTypableOrNot(false);

				this.currentEnemyName = "";
				this.players[this.myPlayerId].setTarget();
				console.log(this.currentEnemyName + "をターゲットから外しました");
				

			}else{
				// console.log("どの敵もターゲットにしていません");
			}
			return true;
		}
		return false;
	}

	changeState(toState, inputValue = ""){
		if(this.gameStartWindow){
			this.gameStartWindow.setDestructionTime(0);
			this.gameStartWindow = null;
		}

		this.currentState = toState;

		switch(toState){
			case 0://難易度選択画面
				this.gameStartWindow = this.createMessageWindow("数字を入力して、<br>難易度を選択してください<br><br>[1]: Easy<br>[2]: Normal<br>[3]: Hard<br>[4]: 遊び方", "Infinity", "small", 13);
				break;
			case 1://ゲームスタート確認画面
				this.setDifficulty(inputValue);
				break;
			case 2://遊び方説明画面
				this.stageIterator = EASY_STAGE_INFORMATION[Symbol.iterator]();
				this.questionArray = HARD_QUESTIONS;
				this.initializeQuestions();

				this.gameStartWindow = this.createMessageWindow("遊び方：<br>　文章を持った敵が上から飛んできます。<br>"+ 
					"　ローマ字で文章を全てタイプすることで、<br>" +
					"　敵を撃ち落とすことができます。<br>" +
					"　時間内にどれだけ敵を倒せるか、<br>"+
					"　あなたのタイピングの腕前が問われます！<br>" +
					"　（ミスによるペナルティはありません）<br>" +
					"<br>[0]:戻る<br>[1]:さらに詳しく", "Infinity", "small", 20);

				//説明用の敵
				if(this.enemiesArray.length == 0){
					this.spawnOneEnemy("enemy", "b0", 50, 30, 25, {sentenceJp:"これが敵です", sentenceRm: "koregatekidesu"});
					this.spawnOneEnemy("enemy", "b4", 50, 30, 25, {sentenceJp:"撃ってね", sentenceRm: "uttene"});
				}

				if(!this.players[this.myPlayerId])
					this.players[this.myPlayerId] = new Player("player");
				break;

			case 3://遊び方説明画面
				this.gameStartWindow = this.createMessageWindow("細かいルール：<br>　スペースキーを押すことで、<br>"+ 
					"　ターゲットにしている敵を、<br>" +
					"　ターゲットから外すことができます。<br>"+ 
					"　他の敵を狙いたいときに有効です！<br>"+ 
					"<br>[0]:戻る", "Infinity", "small", 20);

				//説明用の敵
				if(this.currentEnemyName){
					const enemyLen = this.enemiesArray.length;
					for(let i=0; i<enemyLen; i++){
						this.destroyEnemy(this.enemiesArray[0].getName(), true);
					}
				}
				this.spawnOneEnemy("enemy", "b2", 50, 30, 25, {sentenceJp:"スペースを押してね", sentenceRm: "supe-suwoositene"});
				this.changeTarget(this.pickUpEnemyNumByInitial("s"));

				this.players[this.myPlayerId].createAimScope(this.currentEnemyName, 0);

				break;

			case 10://ゲームスタート！
				const enemyLen = this.enemiesArray.length;
				for(let i=0; i<enemyLen; i++){
					this.destroyEnemy(this.enemiesArray[0].getName(), true);
				}

				console.log("ゲームを開始します");
				this.isPlaying = true;
				if(!this.players[this.myPlayerId])
					this.players[this.myPlayerId] = new Player("player");

				this.initializeEnemyState();
				this.initializeTypeState();
				this.readlineFromStageData();
				break;
		}

	}
	setDifficulty(gameDifficulty){
		this.currentState = 1;
		switch(gameDifficulty){
			case "1":
				this.stageIterator = EASY_STAGE_INFORMATION[Symbol.iterator]();
				this.questionArray = EASY_QUESTIONS;
				this.difficulty = "Easy";
				break;

			case "2":
				this.stageIterator = NORMAL_STAGE_INFORMATION[Symbol.iterator]();
				this.questionArray = NORMAL_QUESTIONS;
				this.difficulty = "Normal";
				break;

			case "3":
				this.stageIterator = HARD_STAGE_INFORMATION[Symbol.iterator]();
				this.questionArray = HARD_QUESTIONS;
				this.difficulty = "Hard";
				break;
		}

		this.initializeQuestions();

		this.gameStartWindow = this.createMessageWindow("難易度" + this.difficulty +"でゲームを開始します<br>(日本語入力をオフにしてから<br>始めてください)<br><br>[スペース]: ゲームを開始<br>[0]: 戻る", "Infinity", "small", 17);
	}
	/*
	*	questionsArray中の、表し方が複数通りある頭文字を、ひとつにまとめます
	*
	*	ex.
	*	文字"ふ"のような複数の表し方がある頭文字("fu" or "hu")を、ひとつの文字にまとめます
	*	(アルファベット"f"から始まる文章は、すべて"h"の文章として扱う)
	*/
	initializeQuestions(){
		//"j"を"z"へ
		Array.prototype.push.apply(this.questionArray[25], this.questionArray[9]);
		this.questionArray[9] = [];
		//"f"を"h"へ
		Array.prototype.push.apply(this.questionArray[7], this.questionArray[5]);
		this.questionArray[5] = [];


		//画面上に同じ頭文字を持つ敵が出現しないようにするための準備
		this.existingInitialArray = [];
		for(var i=0;i<this.questionArray.length;i++){
			this.existingInitialArray[i] = false;

			//文章を持たないアルファベットは、常に選び出されない状態にしておく
			if(this.questionArray[i].length == 0)
				this.existingInitialArray[i] = true;
		}
	}

	/*
	*	指定した番号の敵へターゲットを切り替える
	*	@param {Number} to
	*	@param {Number} from
	*/
	changeTarget(to, from = -1){
		if(!this.enemiesArray[to])
			return false;

		if(this.enemiesArray[from])
			this.enemiesArray[from].changeTypableOrNot(false);
		
		this.enemiesArray[to].changeTypableOrNot(true);
		this.currentEnemyName = this.enemiesArray[to].getName();
		this.setTryingSentence(this.enemiesArray[to].getSentence());

		this.players[this.myPlayerId].setPosX(this.enemiesArray[to].getPos().posX);

		if(this.audioSwitch)
			this.playSwitchSound();

		return true;
	}

	/*
	*	敵配列から指定した名前と同じ名前を持つオブジェクトを見つけ出し、
	*	それが何番目に位置しているかをNumber型で返す
	*
	*	@param {String} enemyName
	*	@return {Number}
	*/
	pickUpEnemyNumByName(enemyName){
		if(!this.enemiesArray)
			return -1;

		for(var i=0;i<this.enemiesArray.length;i++){
			if(this.enemiesArray[i].getName() == enemyName){
				return i;
			}
		}
		return -1;
	}
	pickUpEnemyNumByInitial(enemyInitial){
		if(!this.enemiesArray)
			return -1;

		for(var i=0;i<this.enemiesArray.length;i++){
			if(this.enemiesArray[i].getSentence().sentenceRm.substr(0,1) == enemyInitial){
				return i;
			}
		}
		return -1;	
	}
	pickUpEnemyNameByNum(enemyNum){
		if(this.enemiesArray.length > enemyNum){
			if(this.enemiesArray[enemyNum]){
				return this.enemiesArray[enemyNum].getName();
			}
		}
		return false;
	}

	/*
	*	敵のHPが0になるなどした場合に、
	*	敵オブジェクトに関して、(1)敵配列から削除 (2)対応するエレメントの削除を行う
	*	@param {String} enemyName
	*/
	destroyEnemy(enemyName, isSelfDestruction = false){		
		const beforeEnemyNum = this.pickUpEnemyNumByName(enemyName);
		const beforeEnemyDiv = document.getElementById(enemyName);

		if(!beforeEnemyDiv || beforeEnemyNum == -1)
			return;
		
		if(enemyName == this.currentEnemyName)
			this.currentEnemyName = "";

		this.removeInitialFromArray(beforeEnemyNum);

		//敵配列から指定した敵を削除するが、その前に変数に格納しておく
		let beingDestroyedEnemy = this.enemiesArray[beforeEnemyNum];
		this.enemiesArray.splice(beforeEnemyNum,1);

		if(isSelfDestruction){
			// console.log(enemyName + "は自爆し、あなたにダメージを与えた");
			beforeEnemyDiv.parentNode.removeChild(beforeEnemyDiv);

		}else{
			//スコア関係
			this.totalDestroyedEnemies += 1;

			//エフェクトを見せる時間のため、エレメントを消す前に猶予を与える
			setTimeout(function(){
				beforeEnemyDiv.parentNode.removeChild(beforeEnemyDiv);
			}.bind(this), 300);

			beingDestroyedEnemy.beingDestroyedEffect();
		}


		if(this.isWaitingForExtermination && this.enemiesArray.length == 0){
			this.isWaitingForExtermination = false;
			this.readlineFromStageData();
		}
	}

	removeInitialFromArray(enemyNum){
		if(!this.enemiesArray[enemyNum])
			return;

		const sentenceRm = this.enemiesArray[enemyNum].getSentence().sentenceRm;
		if(sentenceRm){
			const initial = sentenceRm.substr(0,1);
			const initialNum = convertAlphabetCharToDecimalNumber(initial);
			this.existingInitialArray[initialNum] = false;
		}else{
			console.log("消そうとしている敵は、文章オブジェクトを持っていません");
		}
	}

	playSwitchSound(){
		this.audioSwitch[this.audioSwitchCount].play();
		this.audioSwitchCount++;
		if(this.audioSwitchCount > 2)
			this.audioSwitchCount = 0;
	}
}

		
class DebugMode {
	constructor(){
		this.isRunningAutoMode = false;
		this.SEARCH_INTERVAL = 1000;
		this.TYPING_SPEED = 200;

		this.isPaused = false;
		console.warn("デバッグモードが有効になっています");
	}

	callDebugFunction(inputKey){
		if(!gc.isPlaying)
			return;

		if(inputKey == "1"){
			console.log(gc.existingInitialArray);
			return true;
		}
		if(inputKey == "2"){
			if(this.isPaused){
				for(let i=0; i<gc.enemiesArray.length; i++){
					gc.enemiesArray[i].animation.pause();
					this.isPaused = false;
				}	
			}else{
				for(let i=0; i<gc.enemiesArray.length; i++){
					gc.enemiesArray[i].animation.play();
					this.isPaused = true;
				}	
			}

		}
		if(inputKey == "3"){//コンソールにゲームコントローラー各種情報を表示
			console.log("-----------------------------");
			console.log("currentEnemyName: " + gc.currentEnemyName);
			for(var i=0;i<gc.enemiesArray.length;i++){
				if(gc.currentEnemyName == gc.enemiesArray[i].getName()){
					console.log(gc.enemiesArray[i]);
					break;
				}
			}
			console.log("workingSentence: " + gc.workingSentence);
			console.log("sentenceCount: " + gc.sentenceCount);
			console.log("illustrativeAreaJp: " + gc.illustrativeAreaJp);
			return true;
		}
		if(inputKey == "4"){//コンソールに敵配列の情報を表示
			console.log("-----------------------------");
			for(var i=0;i<gc.enemiesArray.length;i++){
				console.log(gc.enemiesArray[i]);
			}
			return true;
		}
		if(inputKey == "5"){//一番最初の敵の文章を変更
			gc.enemiesArray[0].setSentence("テスト", "tesuto");
			return true;
		}
		if(inputKey == "6"){
			return true;
		}
		if(inputKey == "7"){//AUTOモード
			if(!this.isRunningAutoMode){
				this.autoSearchingAndTyping();
				this.isRunningAutoMode = true;
				console.log("AUTOモードを起動しました");
			}else{
				console.log("AUTOモードは既に起動されています");
			}
			return true;
		}
		if(inputKey == "8"){//Gameを終了
			if(gc.isPlaying){
				gc.isPlaying = false;
				console.log("ゲームを終了します");
			}
			return true;
		}
		return false;
	}

	autoSearchingAndTyping(){

		if(gc.enemiesArray.length > 0 && !gc.currentEnemyName){
			const targetEnemy = gc.enemiesArray[0];
			const sentenceObj = targetEnemy.getSentence();

			if(sentenceObj.sentenceRm){
				this.autoTyping(sentenceObj.sentenceRm, 0, this.TYPING_SPEED);
			}else{
				console.error("敵が文章オブジェクトを持っていません");
			}

		}else{
			console.log("敵が見当たらないので、" + this.SEARCH_INTERVAL/1000 + "秒後にもう一度探します");
		}

		if(gc.isPlaying)
			setTimeout("gc.debugMode.autoSearchingAndTyping()", this.SEARCH_INTERVAL);
	}

	autoTyping(string, count, speed = 200){
		gc.onTyped(string[count]);

		if(string.length > count++){
			setTimeout(function(){
				this.autoTyping(string, count, speed);
			}.bind(this), speed);
		}
	}
}


$(function(){
	gc = new GameController();
	gc.inputArea.focus();

	$('#graphic-area').particleground({
		dotColor: '#5cbdaa',
		lineColor: '#5cbdaa',
		directionY: 'down',
		minSpeedY: 2.0,
		maxSpeedY: 3.0,
		density: 20000,
		parallax: false
	});
});