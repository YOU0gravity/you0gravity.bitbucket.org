function createBuggerflyDiv(name){
	const buggerflyDiv = `
	<div class="buggerfly">
		<?xml version="1.0" encoding="utf-8"?>
		<!-- Generator: Adobe Illustrator 20.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
		<svg version="1.1" id="whole-object" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
		<style type="text/css">
			.st0{stroke:#FFFFFF;stroke-miterlimit:10;}
			.st1{fill:none;stroke:#000000;stroke-miterlimit:10;}
		</style>
		<polygon id="Right-wing" class="st0" points="19.6,89.6 34.7,78 34.7,22 19.6,10.4 4.4,22 4.4,78 "/>
		<polygon id="Left-wing" class="st0" points="80.4,89.6 65.3,78 65.3,22 80.4,10.4 95.6,22 95.6,78 "/>
		<g id="Body">
			<line class="st1" x1="39.9" y1="60.4" x2="33" y2="60.4"/>
			<line class="st1" x1="39.9" y1="39.6" x2="33" y2="39.6"/>
			<line class="st1" x1="39.9" y1="50" x2="33" y2="50"/>
			<line class="st1" x1="60.1" y1="60.4" x2="67" y2="60.4"/>
			<line class="st1" x1="60.1" y1="39.6" x2="67" y2="39.6"/>
			<line class="st1" x1="60.1" y1="50" x2="67" y2="50"/>
			<polyline class="st1" points="55.4,74.1 60.2,80.1 55.4,85.7 	"/>
			<polyline class="st1" points="44.6,74.1 39.8,80.1 44.6,85.7 	"/>
			<polygon id="Center" class="st0" points="50,80.1 60.2,70.1 60.2,22.1 50,12.2 39.8,22.1 39.8,70.1 	"/>
			<rect x="53.7" y="84.5" transform="matrix(-1 -9.021729e-11 9.021729e-11 -1 110.5477 172.3427)" class="st0" width="3.2" height="3.2"/>
			<rect x="43" y="84.5" transform="matrix(-1 -8.965685e-11 8.965685e-11 -1 89.2303 172.3427)" class="st0" width="3.2" height="3.2"/>
		</g>
		</svg>
	</div>
	`;

	return buggerflyDiv;
}

function createAimDiv(playerId){
	let color = "##4b9153";
	switch(playerId){
		case 0:
			color = "#4b9153";
			break;
		case 1:
			color = "red";
			break;
		case 2:
			color = "blue";
			break;
		case 3:
			color = "yellow";
			break;
		case 4:
			color = "grey";
			break;
	}
	const aimDiv = `
		<?xml version="1.0" encoding="utf-8"?>
		<!-- Generator: Adobe Illustrator 20.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 200 200" style="enable-background:new 0 0 200 200;" xml:space="preserve">
		<style type="text/css">
		`+
			".st0" + playerId +"{fill:" + color +";}"+
			".st1"+ playerId +"{fill:none;stroke:" + color + ";stroke-miterlimit:10;}"+
			".st2"+ playerId + "{fill:none;stroke:" + color + ";stroke-width:5;stroke-miterlimit:10;}"+
		
		'</style>'+
		'<path class="st0'+ playerId +'" d="M100,8.9C49.7,8.9,8.9,49.7,8.9,100s40.8,91.1,91.1,91.1s91.1-40.8,91.1-91.1S150.3,8.9,100,8.9z M100,186'+
			'c-47.5,0-86-38.5-86-86s38.5-86,86-86s86,38.5,86,86S147.5,186,100,186z"/>'+
		'<path class="st1'+ playerId +'" d="M100,88.7c-6.3,0-11.3,5.1-11.3,11.3c0,6.3,5.1,11.3,11.3,11.3s11.3-5.1,11.3-11.3'+
			'C111.3,93.7,106.3,88.7,100,88.7z M100,110.7c-5.9,0-10.7-4.8-10.7-10.7c0-5.9,4.8-10.7,10.7-10.7s10.7,4.8,10.7,10.7'+
			'C110.7,105.9,105.9,110.7,100,110.7z"/>'+
		'<line class="st2'+ playerId +'" x1="0" y1="100" x2="21.1" y2="100"/>'+
		'<line class="st2'+ playerId +'" x1="178.3" y1="100" x2="199.4" y2="100"/>'+
		'<line class="st2'+ playerId +'" x1="100" y1="177.4" x2="100" y2="198.4"/>'+
		'<line class="st2'+ playerId +'" x1="100" y1="1.3" x2="100" y2="22.4"/>'+
		'</svg>';

	return aimDiv;
}

function createDestroyedDiv(){
	const destroyedDiv = `
		<div class="wound">
			<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">
			<defs><style>.cls-1{fill:#4b9153;}</style></defs>
			<title>wound</title>
			<path class="cls-1" d="M200,0C89.54,0,0,89.55,0,200S89.54,400,200,400s200-89.54,200-200S310.46,0,200,0Zm0,362.08A162.08,162.08,0,1,1,362.08,200,162.08,162.08,0,0,1,200,362.09Z" transform="translate(0 -0.01)"/>
			<path class="cls-1" d="M200,128.44A71.57,71.57,0,1,0,271.57,200,71.57,71.57,0,0,0,200,128.44ZM200,258a58,58,0,1,1,58-58A58,58,0,0,1,200,258Z" transform="translate(0 -0.01)"/>
			</svg>
		</div>
	`;
	return destroyedDiv;
}

//現在仮のSVG画像を利用中
function createBulletDiv(){
	const bulletDiv = `
		<div class="bullet">
			<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 181.77 128.53">
			<defs><style>.cls-1{fill:#231f20;}.cls-2{fill:#4b9153;}</style></defs>
			<title>bullet</title>
			<path class="cls-1" d="M190.89,70.25a128.53,128.53,0,0,0-181.77,0L100,161.14Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-2" d="M179.53,81.62a112.47,112.47,0,0,0-159.05,0L100,161.14Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-1" d="M168.17,93A96.4,96.4,0,0,0,31.83,93L100,161.14Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-2" d="M156.8,104.34a80.33,80.33,0,0,0-113.61,0l56.8,56.8Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-1" d="M145.44,115.7a64.27,64.27,0,0,0-90.89,0L100,161.14Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-2" d="M134.08,127.06a48.2,48.2,0,0,0-68.17,0L100,161.14Z" transform="translate(-9.11 -32.61)"/>
			<path class="cls-1" d="M122.72,138.42a32.13,32.13,0,0,0-45.44,0L100,161.14Z" transform="translate(-9.11 -32.61)"/></svg>
		</div>
		`;
	return bulletDiv;
}

function createPlayerDiv(){
	const playerDiv = `
		<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 195.84 125.09">
		<defs><style>.cls-4,.cls-6{fill:none;stroke:#4b9153;stroke-miterlimit:10;}.cls-4{stroke-width:5px;}.cls-5{fill:#4b9153;}</style></defs>
		<title>Player</title>
		<rect class="cls-4" x="78.77" y="45.97" width="38.31" height="76.62"/>
		<rect class="cls-5" x="86.43" y="61.3" width="22.99" height="45.97"/>
		<rect class="cls-4" x="141.9" y="91.91" width="45.87" height="61.16" transform="translate(285.25 -80.56) rotate(90)"/>
		<rect class="cls-4" x="94.27" y="21.06" width="11.47" height="91.74" transform="translate(164.85 -71.28) rotate(90)"/>
		<rect class="cls-4" x="12.22" y="91.91" width="45.87" height="61.16" transform="translate(155.57 49.12) rotate(90)"/>
		<rect class="cls-5" x="91.79" width="12.26" height="45.97"/>
		<polyline class="cls-6" points="63.66 61.34 78.77 84.28 63.66 107.22"/>
		<polyline class="cls-6" points="132.18 107.22 117.08 84.28 132.18 61.34"/>
		<line class="cls-6" x1="2.5" y1="84.28" x2="63.66" y2="84.28"/>
		<line class="cls-6" x1="132.18" y1="84.28" x2="193.34" y2="84.28"/>
		<line class="cls-6" x1="22.89" y1="61.57" x2="22.89" y2="106.99"/>
		<line class="cls-6" x1="43.27" y1="61.79" x2="43.27" y2="107.22"/>
		<line class="cls-6" x1="172.95" y1="61.57" x2="172.95" y2="106.99"/>
		<line class="cls-6" x1="152.57" y1="61.79" x2="152.57" y2="107.22"/></svg>
	`;
	return playerDiv;
}